import 'package:flutter/material.dart';
import 'package:pdfSlot/View/AuthScreen/loginScreen.dart';
import 'package:pdfSlot/View/AuthScreen/signUpScreen.dart';
import 'package:pdfSlot/View/Others/onBoardingScreen.dart';
import 'package:pdfSlot/View/Others/splashScreen.dart';
import 'package:pdfSlot/View/UserScreens/HomeScreen.dart';
import 'package:pdfSlot/View/UserScreens/mainScreen.dart';

void main() {
  runApp(
    MyApp(),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(fontFamily: 'Balsamiq Sans'),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: <String, WidgetBuilder>{
        '/LoginScreen': (BuildContext context) => LoginScreen(),
        '/SignUpScreen': (BuildContext context) => SignUpScreen(),
        '/MainScreen': (BuildContext context) => MainScreen(),
      },
    );
  }
}
