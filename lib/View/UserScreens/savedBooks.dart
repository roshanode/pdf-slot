import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pdfSlot/View/BookScreens/bookDetails.dart';
import 'package:pdfSlot/View/BookScreens/bookImage.dart';

class SavedBooks extends StatefulWidget {
  final String userId;
  SavedBooks({Key key, @required this.userId}) : super(key: key);
  @override
  _SavedBooksState createState() => _SavedBooksState();
}

class _SavedBooksState extends State<SavedBooks> {
  String uId;
  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();

    this.setState(() {
      uId = user.uid;
    });

    return uId;
  }

  Widget _buildListSavedBooks(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('Books')
            .document(document['BookId'])
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return LinearProgressIndicator(
              backgroundColor: Colors.black12,
            );
          if (snapshot.data == null)
            return LinearProgressIndicator(
              backgroundColor: Colors.black12,
            );
          return Container(
            margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xfffffDBD4B4),
                    Color(0xffffDAE2F8),
                  ],
                ),
                border: Border.all(width: .5),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 0, bottom: 0),
                  height: 150,
                  width: 150,
                  child: BookImage(url: snapshot.data.data['pdfUrl']),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          left: 5,
                        ),
                        child: Text(
                          snapshot.data.data['bookName'],
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.w500),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 5, top: 10, right: 5),
                        child: Text(
                          snapshot.data.data['aboutBook'],
                          style: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w500),
                        ),
                      ),
                      ButtonBar(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BookDetails(
                                          collectionName: collectionName,
                                          documentId: document.documentID,
                                          rating: snapshot.data.data['rating'],
                                          url: snapshot.data.data['pdfUrl'],
                                          bookName:
                                              snapshot.data.data['bookName'],
                                          aboutBook:
                                              snapshot.data.data['aboutBook'],
                                          category:
                                              snapshot.data.data['category'],
                                          postedBy:
                                              snapshot.data.data['postedBy'],
                                          timestamp:
                                              snapshot.data.data['postedOn'])));
                            },
                            child: Text(
                              "View More..",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                      icon: Icon(
                        Icons.remove_circle,
                        color: Colors.red,
                      ),
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Are You Sure ? "),
                              content: Text("You Want To Remove This Book"),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                                FlatButton(
                                  child: Text("Yes"),
                                  onPressed: () async {
                                    Navigator.of(context).pop();
                                    await Firestore.instance
                                        .collection('Users')
                                        .document(uId)
                                        .collection('SavedBooks')
                                        .document(document['BookId'])
                                        .delete();

                                    Fluttertoast.showToast(
                                        msg: "Deleted Successfully",
                                        toastLength: Toast.LENGTH_LONG,
                                        backgroundColor: Colors.green,
                                        textColor: Colors.white);
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xffffCC95C0),
              Color(0xfffffDBD4B4),
              Color(0xfffff7AA1D2)
            ],
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection('Users')
                      .document(uId)
                      .collection('SavedBooks')
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    } else if (snapshot.data.documents.length <= 0) {
                      return Stack(
                        children: [
                          LinearProgressIndicator(
                            backgroundColor: Colors.green,
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            child: Text(
                              "You have't saved any books yet",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.w700),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, index) => _buildListSavedBooks(
                            context,
                            snapshot.data.documents[index],
                            'SavedBooks'),
                      );
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
