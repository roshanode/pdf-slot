import 'package:carousel_pro/carousel_pro.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:pdfSlot/View/BookScreens/bookDetails.dart';
import 'package:pdfSlot/View/BookScreens/bookImage.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  String userId;

  @override
  void initState() {
    super.initState();

    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
        });
      }
    });
  }

  Widget _buildListBooks(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xfffffDBD4B4),
              Color(0xffffDAE2F8),
            ],
          ),
          border: Border.all(width: .5),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 0, bottom: 0),
            height: 150,
            width: 150,
            child: BookImage(url: document['pdfUrl']),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 5,
                  ),
                  child: Text(
                    document['bookName'],
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5, top: 10, right: 5),
                  child: Text(
                    document['aboutBook'],
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(left: 5, top: 10, right: 5),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BookDetails(
                                    collectionName: collectionName,
                                    documentId: document.documentID,
                                    rating: document['rating'],
                                    url: document['pdfUrl'],
                                    bookName: document['bookName'],
                                    aboutBook: document['aboutBook'],
                                    category: document['category'],
                                    postedBy: document['postedBy'],
                                    timestamp: document['postedOn'])));
                      },
                      child: Text(
                        "View More..",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              floating: true,
              pinned: false,
              snap: true,
              stretch: true,
              backgroundColor: Colors.black12,
              flexibleSpace: FlexibleSpaceBar(
                background: FlexibleSpaceBar(
                  centerTitle: true,
                  background: Carousel(
                    boxFit: BoxFit.cover,
                    autoplay: true,
                    animationCurve: Curves.fastOutSlowIn,
                    animationDuration: Duration(milliseconds: 700),
                    dotSize: 3.0,
                    dotIncreasedColor: Colors.red,
                    dotBgColor: Colors.white,
                    dotPosition: DotPosition.bottomCenter,
                    dotVerticalPadding: 0.0,
                    showIndicator: true,
                    indicatorBgPadding: 0,
                    images: [
                      ExactAssetImage("lib/Assets/Images/poster1.jpg"),
                      ExactAssetImage("lib/Assets/Images/poster2.jpg"),
                    ],
                  ),
                ),
              ),
            ),
          ];
        },
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(),
          child: Column(
            children: [
              Expanded(
                child: StreamBuilder(
                    stream: Firestore.instance.collection('Books').snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return LinearProgressIndicator(
                          backgroundColor: Colors.black12,
                        );
                      if (snapshot.data.documents.length <= 0)
                        return Stack(
                          children: [
                            LinearProgressIndicator(
                              backgroundColor: Colors.green,
                            ),
                            Container(
                              alignment: Alignment.center,
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                "There are no books uploaded yet",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w700),
                              ),
                            ),
                          ],
                        );
                      return ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: snapshot.data.documents.length,
                        itemBuilder: (context, index) => _buildListBooks(
                            context, snapshot.data.documents[index], 'Books'),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
