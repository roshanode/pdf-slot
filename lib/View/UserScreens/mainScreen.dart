import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pdfSlot/View/Others/aboutApp.dart';
import 'package:pdfSlot/View/UserScreens/HomeScreen.dart';
import 'package:pdfSlot/View/UserScreens/savedBooks.dart';
import 'package:pdfSlot/View/UserScreens/uploadBook.dart';
import 'package:pdfSlot/View/UserScreens/userProfileScreen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  String userId;
  @override
  void initState() {
    super.initState();
    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
        });
      }
    });
    _tabController = TabController(length: 5, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromARGB(0xff, 102, 57, 182),
            flexibleSpace: Container(
              padding: EdgeInsets.only(top: 5, left: 5, right: 5),
              margin: EdgeInsets.only(top: 10, left: 10, right: 10),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(5)),
              width: MediaQuery.of(context).size.width,
              child: TextFormField(
                enabled: false,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      size: 30,
                      color: Colors.black,
                    ),
                    prefixIcon: Icon(Icons.book),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: "Search Your Book By Category",
                    hintStyle: TextStyle(
                      fontSize: 18,
                    )),
              ),
            ),
            bottom: TabBar(
              controller: _tabController,
              labelColor: Colors.green,
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorWeight: 3.0,
              indicatorColor: Colors.green,
              unselectedLabelColor: Colors.white,
              tabs: <Widget>[
                Tab(
                  icon: FaIcon(FontAwesomeIcons.home),
                  text: "Home",
                ),
                Tab(
                  icon: Icon(Icons.collections_bookmark),
                  text: "Saved",
                ),
                Tab(
                  icon: Icon(Icons.share),
                  text: "Share",
                ),
                Tab(
                  icon: Icon(Icons.person),
                  text: "Profile",
                ),
                Tab(
                  icon: Icon(Icons.info),
                  text: "About",
                ),
              ],
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: <Widget>[
              HomeScreen(),
              SavedBooks(
                userId: userId,
              ),
              UploadBook(),
              UserProfile(),
              AboutApp(),
            ],
          ),
        ),
      ),
    );
  }
}
