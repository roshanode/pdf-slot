import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pdfSlot/View/BookScreens/bookDetails.dart';
import 'package:pdfSlot/View/BookScreens/bookImage.dart';

class SharedBooks extends StatefulWidget {
  final String userId;
  SharedBooks({@required this.userId});
  @override
  _SharedBooksState createState() => _SharedBooksState();
}

class _SharedBooksState extends State<SharedBooks> {
  Widget _buildListSharedBooks(
      BuildContext context, DocumentSnapshot document, String collectionName) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xfffffDBD4B4),
              Color(0xffffDAE2F8),
            ],
          ),
          border: Border.all(width: .5),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 0, bottom: 0),
            height: 150,
            width: 150,
            child: BookImage(url: document['pdfUrl']),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    left: 5,
                  ),
                  child: Text(
                    document['bookName'],
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5, top: 10, right: 5),
                  child: Text(
                    document['aboutBook'],
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500),
                  ),
                ),
                ButtonBar(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => BookDetails(
                                    collectionName: collectionName,
                                    documentId: document.documentID,
                                    rating: document['rating'],
                                    url: document['pdfUrl'],
                                    bookName: document['bookName'],
                                    aboutBook: document['aboutBook'],
                                    category: document['category'],
                                    postedBy: document['postedBy'],
                                    timestamp: document['postedOn'])));
                      },
                      child: Text(
                        "View More..",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.solidTrashAlt,
                  color: Colors.red,
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text("Are You Sure ? "),
                        content: Text("You Want To Delete This Book"),
                        actions: <Widget>[
                          FlatButton(
                            child: Text("No"),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          FlatButton(
                            child: Text("Yes"),
                            onPressed: () async {
                              Navigator.of(context).pop();
                              await FirebaseStorage.instance
                                  .getReferenceFromUrl(document['pdfUrl'])
                                  .then((value) => value.delete())
                                  .catchError((onError) {
                                print(onError);
                              });
                              await Firestore.instance
                                  .collection('Books')
                                  .document(document.documentID)
                                  .delete();

                              Fluttertoast.showToast(
                                  msg: "Deleted Successfully",
                                  toastLength: Toast.LENGTH_LONG,
                                  backgroundColor: Colors.green,
                                  textColor: Colors.white);
                            },
                          ),
                        ],
                      );
                    },
                  );
                }),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 35,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        centerTitle: true,
        title: Text(
          "Books You Shared",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff2193b0), Color(0xffff6dd5ed)])),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[
              Color(0xffffCC95C0),
              Color(0xfffffDBD4B4),
              Color(0xfffff7AA1D2)
            ],
          ),
        ),
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection('Books')
                      .where('postedBy', isEqualTo: widget.userId)
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.black12,
                      );
                    if (snapshot.data == null)
                      return LinearProgressIndicator(
                        backgroundColor: Colors.green,
                      );
                    return ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _buildListSharedBooks(
                          context,
                          snapshot.data.documents[index],
                          'SavedBooks'),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
