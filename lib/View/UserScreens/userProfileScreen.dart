import 'dart:io';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:path/path.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pdfSlot/View/UserScreens/sharedBooks.dart';
import 'package:url_launcher/url_launcher.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String userId;
  final nameController = TextEditingController();
  String photoUrl;
  File avatarImageFile;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();

    FirebaseAuth.instance.currentUser().then((firebaseUser) {
      if (firebaseUser != null) {
        setState(() {
          userId = firebaseUser.uid;
          print("Logged in user ID:- " + userId);
        });
      }
    });
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        avatarImageFile = image;
      });
      uploadFile();
    }
  }

  takePicture() async {
    File image = await ImagePicker.pickImage(source: ImageSource.camera);

    if (image != null) {
      avatarImageFile = image;
      setState(() {});
      uploadFile();
    }
  }

  void chosePicture(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                    leading: FaIcon(FontAwesomeIcons.camera),
                    title: Text('Take Picture'),
                    onTap: () {
                      takePicture();
                      Navigator.of(context).pop();
                    }),
                ListTile(
                  leading: FaIcon(FontAwesomeIcons.images),
                  title: Text('Choose Picture'),
                  onTap: () {
                    getImage();
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          );
        });
  }

//delete previous pic
  Future deleteImage() async {
    await FirebaseStorage.instance
        .getReferenceFromUrl(photoUrl)
        .then((value) => value.delete())
        .catchError((onError) {
      print(onError);
    });
  }

  Future uploadFile() async {
    setState(() {
      isLoading = true;
    });
    String fileName = basename(avatarImageFile.path);
    StorageReference reference =
        FirebaseStorage.instance.ref().child('userProfile').child(fileName);
    StorageUploadTask uploadTask = reference.putFile(avatarImageFile);
    StorageTaskSnapshot storageTaskSnapshot;
    uploadTask.onComplete.then((value) {
      if (value.error == null) {
        storageTaskSnapshot = value;
        storageTaskSnapshot.ref.getDownloadURL().then((downloadUrl) {
          deleteImage();
          photoUrl = downloadUrl;

          Firestore.instance.collection('Users').document(userId).updateData({
            'photoUrl': photoUrl,
          }).then((data) async {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: "Upload success");
          }).catchError((err) {
            setState(() {
              isLoading = false;
            });
            Fluttertoast.showToast(msg: err.toString());
          });
        }, onError: (err) {
          setState(() {
            isLoading = false;
          });
          Fluttertoast.showToast(msg: 'This file is not an image');
        });
      } else {
        setState(() {
          isLoading = false;
        });
        Fluttertoast.showToast(msg: 'This file is not an image');
      }
    }, onError: (err) {
      setState(() {
        isLoading = false;
      });
      Fluttertoast.showToast(msg: err.toString());
    });
  }

  Widget _buildProfileCard(BuildContext context, snapshot) {
    nameController.text = snapshot.data.data['fullName'];
    photoUrl = snapshot.data.data['photoUrl'];
    return Container(
      margin: EdgeInsets.only(top: 10.0, left: 20, right: 20.0),
      // height: MediaQuery.of(context).size.height / 6,
      decoration: BoxDecoration(
        color: Colors.black12,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(width: 1, color: Colors.black.withOpacity(.5)),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 0,
            child: Container(
              height: 90,
              width: 90,
              child: (snapshot.data.data['photoUrl'] != null)
                  ? Stack(
                      children: [
                        (avatarImageFile == null)
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: CachedNetworkImage(
                                  imageUrl: snapshot.data.data['photoUrl'],
                                  fit: BoxFit.cover,
                                  height: 90,
                                  width: 90,
                                  placeholder: (context, url) => Container(
                                    height: 90,
                                    width: 90,
                                    child: CircularProgressIndicator(
                                      backgroundColor: Colors.white,
                                    ),
                                  ),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                ),
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.file(
                                  avatarImageFile,
                                  height: 90,
                                  width: 90,
                                ),
                              ),
                        Positioned(
                            bottom: 0,
                            right: 0,
                            left: 0,
                            child: IconButton(
                                icon: FaIcon(
                                  FontAwesomeIcons.cameraRetro,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  chosePicture(context);
                                }))
                      ],
                    )
                  : Container(
                      child: Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          backgroundColor: Colors.blue,
                          strokeWidth: 2,
                        ),
                      ),
                    ),
            ),
          ),
          Expanded(
            flex: 0,
            child: Container(
              margin: EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: (snapshot.data.data['fullName'] != null)
                        ? (Text(
                            snapshot.data.data['fullName'],
                            style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600),
                          ))
                        : Container(
                            child: Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.blue,
                                strokeWidth: 2,
                              ),
                            ),
                          ),
                  ),
                  Container(
                    child: (snapshot.data.data['email'] != null)
                        ? (Text(snapshot.data.data['email'],
                            style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.black,
                                fontWeight: FontWeight.w600)))
                        : Container(
                            child: Align(
                              alignment: Alignment.center,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.blue,
                                strokeWidth: 1,
                              ),
                            ),
                          ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<Null> handleSignOut() async {
    try {
      await FirebaseAuth.instance.signOut();

      await GoogleSignIn().signOut();
      Navigator.of(this.context).pushReplacementNamed('/LoginScreen');
    } catch (e) {
      print(e.message);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black12,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  StreamBuilder(
                    stream: Firestore.instance
                        .collection('Users')
                        .document(userId)
                        .snapshots(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData)
                        return Container(
                          alignment: Alignment.center,
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.black12,
                          ),
                        );
                      if (snapshot.data.data == null)
                        return Container(
                          alignment: Alignment.center,
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.black12,
                          ),
                        );
                      return _buildProfileCard(context, snapshot);
                    },
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1)),
                    child: Center(
                      child: ListTile(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      SharedBooks(userId: userId)));
                        },
                        isThreeLine: false,
                        leading: Icon(
                          Icons.share,
                          size: 40,
                          color: Colors.blue,
                        ),
                        trailing: Icon(Icons.chevron_right),
                        title: Text("Shared Books"),
                        subtitle: Text("The Books You Shared"),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(width: 1)),
                    child: Center(
                      child: ListTile(
                        onTap: () {
                          handleSignOut();
                        },
                        isThreeLine: false,
                        leading: Icon(
                          Icons.exit_to_app,
                          size: 40,
                          color: Colors.blue,
                        ),
                        trailing: Icon(Icons.chevron_right),
                        title: Text("Log Out"),
                        subtitle: Text("Log Out from the app"),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            child: isLoading
                ? Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xfff5a623))),
                    ),
                    color: Colors.black.withOpacity(0.5),
                  )
                : Container(),
          ),
        ],
      ),
    );
  }
}
