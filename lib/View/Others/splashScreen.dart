import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:pdfSlot/View/AuthScreen/loginScreen.dart';
import 'package:pdfSlot/View/Others/onBoardingScreen.dart';
import 'package:pdfSlot/View/UserScreens/mainScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool firstTime = false;
  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 2)).then(
      (val) async {
        FirebaseAuth _auth = FirebaseAuth.instance;

        var user = await _auth.currentUser();
        SharedPreferences prefs = await SharedPreferences.getInstance();

        if (prefs.getBool('isFirstTime') == null) {
          setState(() {
            firstTime = true;
          });
        }

        if (user != null) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => MainScreen()));
        } else {
          if (firstTime) {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => OnBoardingScreen()));
          } else {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          }
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0xffffC9D6FF), Color(0xffffE2E2E2)])),
        child: SafeArea(
          child: new LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 200,
                    width: 200,
                    child: Column(
                      children: [
                        Expanded(
                          child: Image.asset(
                            'lib/Assets/Images/logo.png',
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Text(
                            "PDF Slot",
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20),
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.grey,
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
