import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutApp extends StatefulWidget {
  @override
  _AboutAppState createState() => _AboutAppState();
}

class _AboutAppState extends State<AboutApp> {
  void _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "About This App",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                  ),
                  Expanded(
                    child: Text(
                      "Pdf Slot is a simple android application developed by Roshan Shrestha. It aims to provide a digital solution for book lovers. Through this app the users can read different PDF uploaded by other users and they can also share the PDf format of books with other.",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              thickness: 2,
              color: Colors.black,
            ),
            ListTile(
              onTap: () {
                _launchURL('https://www.facebook.com/roshandroids/');
              },
              leading: Image.asset(
                "lib/Assets/Images/facebook.png",
                height: 40,
              ),
              title: Text(
                "Roshan Shrestha",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              subtitle: Text(
                "facebook.com/roshandroids/",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              trailing: FaIcon(
                FontAwesomeIcons.link,
                color: Colors.blue,
                size: 25,
              ),
            ),
            ListTile(
              onTap: () {
                _launchURL('https://www.instagram.com/roshandroids/');
              },
              leading: Image.asset(
                "lib/Assets/Images/instagram.png",
                height: 40,
              ),
              title: Text(
                "Roshan Shrestha",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              subtitle: Text(
                "instagram.com/roshandroids/",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              trailing: FaIcon(
                FontAwesomeIcons.link,
                color: Colors.blue,
                size: 25,
              ),
            ),
            ListTile(
              onTap: () {
                _launchURL('https://twitter.com/Roshandroids');
              },
              leading: Image.asset(
                "lib/Assets/Images/twitter.png",
                height: 40,
              ),
              title: Text(
                "Roshan Shrestha",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              subtitle: Text(
                "twitter.com/Roshandroids",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              trailing: FaIcon(
                FontAwesomeIcons.link,
                color: Colors.blue,
                size: 25,
              ),
            ),
            ListTile(
              onTap: () {
                _launchURL(
                    'https://www.linkedin.com/in/roshan-shrestha-317bb3156/');
              },
              leading: Image.asset(
                "lib/Assets/Images/linkedin.png",
                height: 40,
              ),
              title: Text(
                "Roshan Shrestha",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              subtitle: Text(
                "linkedin.com/in/roshan-shrestha-317bb3156/",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              trailing: FaIcon(
                FontAwesomeIcons.link,
                color: Colors.blue,
                size: 25,
              ),
            ),
            ListTile(
              onTap: () {
                _launchURL('https://github.com/roshandroids');
              },
              leading: Image.asset(
                "lib/Assets/Images/github.png",
                height: 40,
              ),
              title: Text(
                "Roshan Shrestha",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
              subtitle: Text(
                "github.com/roshandroids",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
              ),
              trailing: FaIcon(
                FontAwesomeIcons.link,
                color: Colors.blue,
                size: 25,
              ),
            )
          ],
        ),
      ),
    );
  }
}
