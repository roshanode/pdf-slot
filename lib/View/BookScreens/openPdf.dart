import 'dart:io';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:flutter/material.dart';

class OpenPdf extends StatefulWidget {
  final String title;
  final String url;
  OpenPdf({Key key, @required this.url, @required this.title})
      : super(key: key);
  @override
  _OpenPdfState createState() => _OpenPdfState();
}

class _OpenPdfState extends State<OpenPdf> {
  FileInfo fileInfo;
  int _totalPages = 0;
  int _currentPage = 0;
  String localPath;
  PDFViewController _pdfViewController;
  @override
  void initState() {
    super.initState();
    loadPDF();
  }

  void loadPDF() async {
    File f = await DefaultCacheManager().getSingleFile(widget.url);

    setState(() {
      localPath = f.path;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.chevron_left,
              size: 35,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        centerTitle: true,
        title: Text(
          widget.title,
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        actions: [
          Center(
              child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              _currentPage.toString() + "/" + _totalPages.toString(),
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
          ))
        ],
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: <Color>[Color(0xffff2193b0), Color(0xffff6dd5ed)])),
        ),
      ),
      body: Center(
        child: (localPath != null)
            ? PDFView(
                fitEachPage: true,
                fitPolicy: FitPolicy.BOTH,
                pageSnap: true,
                swipeHorizontal: true,
                autoSpacing: true,
                enableSwipe: true,
                filePath: localPath,
                onError: (e) {
                  print(e);
                },
                onRender: (_pages) {
                  setState(() {
                    _totalPages = _pages;
                  });
                },
                onViewCreated: (PDFViewController vc) {
                  _pdfViewController = vc;
                },
                onPageChanged: (int page, int total) {
                  setState(() {});
                },
                onPageError: (page, e) {},
              )
            : CircularProgressIndicator(),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _currentPage > 0
              ? FloatingActionButton.extended(
                  backgroundColor: Colors.red,
                  label: Text("Go to ${_currentPage - 1}"),
                  onPressed: () {
                    _currentPage -= 1;
                    _pdfViewController.setPage(_currentPage);
                  },
                )
              : Offstage(),
          _currentPage + 1 < _totalPages
              ? FloatingActionButton.extended(
                  backgroundColor: Colors.green,
                  label: Text("Go to ${_currentPage + 1}"),
                  onPressed: () {
                    _currentPage += 1;
                    _pdfViewController.setPage(_currentPage);
                  },
                )
              : Offstage(),
        ],
      ),
    );
  }
}
