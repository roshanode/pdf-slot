import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pdfSlot/View/BookScreens/bookImage.dart';
import 'package:pdfSlot/View/BookScreens/openPdf.dart';
import 'package:timeago/timeago.dart' as timeago;

class BookDetails extends StatefulWidget {
  final String bookName;
  final String aboutBook;
  final String category;
  final String postedBy;
  final Timestamp timestamp;
  final int rating;
  final String collectionName;
  final String documentId;
  final url;
  BookDetails(
      {Key key,
      @required this.bookName,
      @required this.aboutBook,
      @required this.rating,
      @required this.category,
      @required this.collectionName,
      @required this.documentId,
      @required this.url,
      @required this.postedBy,
      @required this.timestamp})
      : super(key: key);
  @override
  _BookDetailsState createState() => _BookDetailsState();
}

class _BookDetailsState extends State<BookDetails> {
  final reviewController = TextEditingController();
  final databaseReference = Firestore.instance;
  String uId;
  String userName;
  String image;

  String loggedInUserType;
  @override
  void initState() {
    super.initState();
    userData();
  }

  Future<String> userData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();

    this.setState(() {
      uId = user.uid;
    });
    getUserType();

    return uId;
  }

  void getUserType() async {
    await Firestore.instance
        .collection('Users')
        .document(uId)
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        userName = ds.data['fullName'];

        image = ds.data['photoUrl'];
      });
    });
  }

  void postReview() async {
    if (reviewController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: "Write Something", backgroundColor: Colors.red);
    } else {
      setState(() {
        FocusScope.of(context).requestFocus(new FocusNode());
      });

      await Firestore.instance
          .collection("Books")
          .document(widget.documentId)
          .collection("Reviews")
          .add({
        "commentedBy": userName,
        "comment": reviewController.text,
        "image": image,
        "time": DateTime.now(),
      });
      setState(() {
        reviewController.clear();
        Fluttertoast.showToast(
            msg: "Review Submitted !", backgroundColor: Colors.green);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color.fromARGB(0xff, 102, 57, 182),
        actions: [
          Icon(Icons.more_vert),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  color: Color.fromARGB(0xff, 102, 57, 182),
                  height: MediaQuery.of(context).size.height / 3.2,
                  width: MediaQuery.of(context).size.width,
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  height: MediaQuery.of(context).size.height / 3,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Container(
                          child: BookImage(url: widget.url),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20, left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.bookName,
                                style: TextStyle(
                                    fontSize: 24,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 0),
                                child: Text(
                                  widget.category,
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 0),
                                child: RatingBarIndicator(
                                  rating: widget.rating.toDouble(),
                                  itemBuilder: (context, index) => Icon(
                                    Icons.star,
                                    color: Colors.amber,
                                  ),
                                  itemCount: 5,
                                  itemSize: 20.0,
                                  direction: Axis.horizontal,
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 0),
                                child: Text(
                                  "Shared By :",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 0),
                                child: StreamBuilder(
                                    stream: Firestore.instance
                                        .collection('Users')
                                        .document(widget.postedBy)
                                        .snapshots(),
                                    builder: (context, snapshot) {
                                      if (!snapshot.hasData)
                                        return LinearProgressIndicator(
                                          backgroundColor: Colors.green,
                                        );
                                      return Text(
                                        snapshot.data.data['fullName'],
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500),
                                      );
                                    }),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(top: 10, left: 0),
                                child: Text(
                                  timeago.format(widget.timestamp.toDate()),
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10, top: 5),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "About The Book",
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10, top: 5),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      widget.aboutBook,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    height: 50,
                    padding: EdgeInsets.only(left: 10, top: 5, right: 10),
                    margin: EdgeInsets.only(top: 10),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => OpenPdf(
                                            title: widget.bookName,
                                            url: widget.url,
                                          )));
                            },
                            child: Container(
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.black12,
                                  borderRadius: BorderRadius.circular(10)),
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                "Read Now",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                        Expanded(
                          child: StreamBuilder(
                              stream: Firestore.instance
                                  .collection('Users')
                                  .document(uId)
                                  .collection('SavedBooks')
                                  .document(widget.documentId)
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (!snapshot.hasData) {
                                  return Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.black12,
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    margin: EdgeInsets.only(right: 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "Read Now",
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        Icon(
                                          Icons.bookmark_border,
                                          color: Colors.black,
                                        )
                                      ],
                                    ),
                                  );
                                } else {
                                  return InkWell(
                                    onTap: () {
                                      Firestore.instance
                                          .collection('Users')
                                          .document(uId)
                                          .collection('SavedBooks')
                                          .document(widget.documentId)
                                          .setData(
                                              {"BookId": widget.documentId});
                                    },
                                    child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            color: Colors.black12,
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                        margin: EdgeInsets.only(right: 10),
                                        child: (snapshot.data.data == null)
                                            ? Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Read Now",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  Icon(
                                                    Icons.bookmark_border,
                                                    color: Colors.black,
                                                  )
                                                ],
                                              )
                                            : Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Text(
                                                    "Already Saved",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w600),
                                                  ),
                                                  Icon(
                                                    Icons.bookmark,
                                                    color: Colors.red,
                                                  )
                                                ],
                                              )),
                                  );
                                }
                              }),
                        )
                      ],
                    ),
                  ),
                  Divider(
                    indent: 10,
                    endIndent: 10,
                    thickness: 1,
                    color: Colors.black,
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 10, bottom: 20),
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width / 1.7,
                            child: TextFormField(
                              controller: reviewController,
                              cursorColor: Colors.black,
                              decoration: new InputDecoration(
                                hintText: "Write a review...",
                                hintStyle: TextStyle(),
                              ),
                            ),
                          ),
                        ),
                        IconButton(
                          icon: FaIcon(FontAwesomeIcons.paperPlane),
                          onPressed: () {
                            if (reviewController.text.isNotEmpty) {
                              postReview();
                            } else {
                              Fluttertoast.showToast(msg: "Write Something");
                            }
                          },
                          color: Colors.blue[800],
                          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                          splashColor: Colors.grey,
                        )
                      ],
                    ),
                  ),
                  StreamBuilder(
                      stream: Firestore.instance
                          .collection('Books')
                          .document(widget.documentId)
                          .collection('Reviews')
                          .orderBy("time", descending: false)
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return LinearProgressIndicator(
                            backgroundColor: Colors.green,
                          );
                        } else if (snapshot.data.documents.length <= 0) {
                          return Container(
                            child: Center(
                              child: Text(
                                "No Reviews yet",
                                style: TextStyle(),
                              ),
                            ),
                          );
                        } else if (snapshot.data != null) {
                          List<Reviews> reviews = [];
                          snapshot.data.documents.forEach((doc) {
                            reviews.add(Reviews.fromDocument(doc));
                          });
                          return Column(
                            children: reviews,
                          );
                        } else {
                          return Container();
                        }
                      }),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Reviews extends StatelessWidget {
  final String userName;
  final String usertype;
  final String image;
  final String comment;
  final Timestamp timestamp;

  Reviews(
      {this.userName, this.usertype, this.image, this.comment, this.timestamp});

  factory Reviews.fromDocument(DocumentSnapshot doc) {
    return Reviews(
      userName: doc['commentedBy'],
      usertype: doc['userType'],
      image: doc['image'],
      comment: doc['comment'],
      timestamp: doc['time'],
    );
  }

  @override
  Widget build(BuildContext context) {
    String s = userName;
    var pos = s.lastIndexOf(' ');
    String result = (pos != -1) ? s.substring(0, pos) : s;

    return Column(
      children: <Widget>[
        ListTile(
          title: Row(
            children: <Widget>[
              Flexible(
                child: (userName == null)
                    ? CircularProgressIndicator()
                    : Text(
                        result,
                        style: TextStyle(),
                      ),
              ),
            ],
          ),
          leading: CircleAvatar(
            backgroundImage: CachedNetworkImageProvider(image),
          ),
          subtitle: Text(
            comment,
            style: TextStyle(),
          ),
          trailing: Text(
            timeago.format(timestamp.toDate()),
            style: TextStyle(),
          ),
        ),
        Divider(
          thickness: 1,
          color: Colors.black,
        ),
      ],
    );
  }
}
